////////////////////////////////////////////////
// KKO                            KKO project //
// 05.5.2023       Vojtěch Kulíšek (xkulis03) //
// Code or decode data with huffman tree      //
////////////////////////////////////////////////

#define SMALLEST_UNDEFINED 257

/*
 * return index of minimal of symbol from 257 long array of size_t,
 * without zero symbol
 *
 * array - input array for search
 */
uint16_t huffman_get_smallest(size_t *array){
    
    size_t smallest = ERROR;
    uint16_t index = SMALLEST_UNDEFINED;
    for(uint16_t i=0;i<256;i++){
        if(array[i]<smallest && array[i] != 0){
            smallest = array[i];
            index = i;
        }
    }

    return index;
}

/*
 * construct huffman tree with recursion
 *
 * symbols - number of times the symbol appears
 * sizes - return size of symbols lenght in huffman tree
 */
void huffman_compute_symbol_sizes(size_t *symbols, uint16_t *sizes){

    // get first symbol
    uint16_t smallest = huffman_get_smallest(symbols);
    size_t old_smalest = symbols[smallest];
    symbols[smallest] = 0;
    // get second symbol
    uint16_t smallest2 = huffman_get_smallest(symbols);

    if(smallest2 != SMALLEST_UNDEFINED){

        // change to new imaginary symbol
        symbols[smallest] = old_smalest+symbols[smallest2];
        symbols[smallest2] = 0;

        huffman_compute_symbol_sizes(symbols, sizes);

        // increment size of symbol
        sizes[smallest] += 1;
        sizes[smallest2] = sizes[smallest];
    }
    
    return;
}

/*
 * short data by values bits lenght
 *
 * data - bites lenght
 * byte - lenght reprezents
 */
void huffman_short(uint16_t *data, uint8_t *byte){

    for(uint16_t i=0;i<256;i++){
        byte[i] = i;
    }
    
    for(uint16_t i=0;i<255;i++){
        for(uint16_t j=0;j<(255-i);j++){
            if(data[j] > data[j+1]){
                uint8_t old = data[j+1];
                data[j+1] = data[j];
                data[j] = old;
                uint8_t old_byte = byte[j+1];
                byte[j+1] = byte[j];
                byte[j] = old_byte;
            }
        }
    }
}

/*
 * code or decode input data to huffman codding
 *
 * buffer - input data, !!! input buffer must be +260 byte bigger thean input data
 * size_of_data - size of input data
 * out_buffer - mallocated buffer with output data
 * code - if true code to huffman codding otherwise decode
 * return byte size of coded data, coded data is returned in buffer
 */
size_t huffman(uint8_t *buffer, size_t size_of_data, uint8_t **out_buffer, bool code){

    if(code){

        *out_buffer = malloc(size_of_data*sizeof(uint8_t)+260);
        if(*out_buffer == NULL){
            write(STD_ERR, "Out of memory.\n", 15);
            return EXIT_FAILURE;
        }
        bite_write_init(*out_buffer);

        // code to huffman codec

        // count size of all symbols

        size_t combinations[256] = {0};
        for(size_t i=0;i<size_of_data;i++){
            combinations[buffer[i]] += 1;
        }
        uint16_t sizes[256] = {0};
        huffman_compute_symbol_sizes(combinations, sizes);

        // write header to output
        bite_write((0b11111111000000000000000000000000&size_of_data)>>24, 8);
        bite_write((0b00000000111111110000000000000000&size_of_data)>>16, 8);
        bite_write((0b00000000000000001111111100000000&size_of_data)>>8, 8);
        bite_write(0b00000000000000000000000011111111&size_of_data, 8);
        
        for(uint16_t i=0;i<256;i++){
            bite_write(sizes[i], 8);
        }

        uint8_t symbols[256];
        huffman_short(sizes, symbols);

        huffman_tree_create_from_values_array(symbols, sizes);

        huffman_tree_convert_input_to_code_and_write_to_buffer(buffer, size_of_data);

        return bite_write_get_bite_writed_bytes();

    } else{

        size_t size_of_data = 0;
        size_of_data |= buffer[0];
        size_of_data <<= 8;
        size_of_data |= buffer[1];
        size_of_data <<= 8;
        size_of_data |= buffer[2];
        size_of_data <<= 8;
        size_of_data |= buffer[3];
        
        // create tree
        uint16_t sizes[256] = {0};
        for(uint16_t i=0;i<256;i++){
            sizes[i] = buffer[4+i];
        }
        uint8_t symbols[256];
        huffman_short(sizes, symbols);

        huffman_tree_create_from_values_array(symbols, sizes);

        *out_buffer = malloc(size_of_data*sizeof(uint8_t));
        if(*out_buffer == NULL){
            write(STD_ERR, "Out of memory.\n", 15);
            return EXIT_FAILURE;
        }

        // conver from code to raw data
        huffman_convert_input_from_code(&(buffer[260]), size_of_data, *out_buffer);

        return size_of_data;

    }
}