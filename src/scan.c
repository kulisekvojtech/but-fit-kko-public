////////////////////////////////////////////////
// KKO                            KKO project //
// 05.5.2023       Vojtěch Kulíšek (xkulis03) //
// Code or decode data with scanning          //
////////////////////////////////////////////////

#define SCAN_DYNAMIC 0
#define SCAN_STATIC 1
#define SCAN_NONE 2

#define SCAN_HEADER_SIZE 5
#define MODEL_HEADER 1

#define SCAN_BLOCK 16
#define SCAN_BLOCK_SIZE SCAN_BLOCK*SCAN_BLOCK
#define SCAN_BLOCK_HEADER 1

#define SCAN_BLOCK_HORIZONTALY 0
#define SCAN_BLOCK_VERTICALY 1
#define SCAN_BLOCK_NONE 255

/*
 * read scan header form array
 *
 * header - array for read
 * width - return image width
 * height - return image height
 * type - return scan type
 */
void scan_read_header(uint8_t *header, uint16_t *width, uint16_t *height, uint8_t *type){
        
        *width = 0;
        *height = 0;

        // scan type header
        *type =  header[0] >> 1;

        // image size header
        *width |= header[1];
        *width <<= 8;
        *width |= header[2];

        *height |= header[3];
        *height <<= 8;
        *height |= header[4];
}

/*
 * write scan header to array
 *
 * old_header - old header with (model header)
 * new_header - array for write
 * width - image width
 * height - return image height
 * type - scan type
 */
void scan_write_header(uint8_t *old_header, uint8_t *new_header, uint16_t width, uint16_t height, uint8_t type){

    // scan type header
    new_header[0] = old_header[0] | type <<1;

    // image size header
    new_header[1] = (0b00000000000000001111111100000000&width)>>8;
    new_header[2] = (0b00000000000000000000000011111111&width);
    new_header[3] = (0b00000000000000001111111100000000&height)>>8;
    new_header[4] = 0b00000000000000000000000011111111&height;

}

/*
 * code input data with static scan
 *
 * in_data - pointer to array of data
 * data_size - count of data to encode
 * out_data - out encoded data
 * return size of output data
 */
ssize_t scan_static_encode(uint8_t *in_data, size_t data_size, uint8_t *out_data){

    // use static scan coding
    size_t count = 0;
    size_t out_data_pointer = 0;
    uint8_t symbol = in_data[0];
    for(size_t i=1;i<data_size;i++){
        if(symbol != in_data[i] || count == 255){
            out_data[out_data_pointer] = symbol;
            out_data[out_data_pointer+1] = count;
            out_data_pointer += 2;
            symbol = in_data[i];
            count = 0;
        }else{
            count += 1;
        }
    }

    // write last symbol
    out_data[out_data_pointer] = symbol;
    out_data[out_data_pointer+1] = count;
    out_data_pointer += 2;

    return out_data_pointer;
}

/*
 * code input data with static scan
 *
 * in_data - pointer to array of data
 * data_size - count of input data for decode
 * out_data - out decoded data
 * mallocated_size - out_data max size
 * space_for_header - non encoded space before encoded data in out_data
 * return size of output data or readed data if output data is bigger thean max_size
 */
size_t scan_static_decode(uint8_t *in_data, size_t data_size, uint8_t *out_data, size_t mallocated_size, size_t space_for_header){
    
    size_t out_data_pointer = space_for_header;

    memcpy(out_data, in_data, sizeof(uint8_t)*space_for_header);

    // decode static scan
    for(size_t i=space_for_header;i<data_size;i+=2){

        // if allocated low memory
        if(out_data_pointer > mallocated_size){
            return i;
        }

        // read scan tag <symbol|lenght>
        uint8_t symbol = in_data[i];
        uint16_t len = in_data[i+1]+1;

        // decode all tags
        for(uint16_t j=0;j<len;j++){

            out_data[out_data_pointer] = symbol;
            out_data_pointer += 1;
        }

    }

    return out_data_pointer;
}

/*
 * get block width and height
 * 
 * column - column of block 
 * row - row of block
 * width - width of image
 * height - height of image
 * end_width - out block width
 * end_height - out block height 
 */
void scan_get_block_size(size_t column, size_t row, uint16_t width, uint16_t height, uint16_t *end_width, uint16_t *end_height){

    // block width
    size_t start_column = column;
    *end_width = column+SCAN_BLOCK;
    if(*end_width > width){
        *end_width = width;
    }

    // block height
    size_t start_row = row;
    *end_height = row+SCAN_BLOCK;
    if(*end_height > height){
        *end_height = height;
    }

}

/*
 * read block from image
 *
 * in_data - input raw data of image
 * column - column of block position
 * row - row of block position
 * width - width of image
 * height - height of image
 * out_data - encoded out data
 * type - type of read scan_BLOCK_HORIZONTALY or scan_BLOCK_VERTICALY
 * return size of block
 */
size_t scan_read_block(uint8_t *in_data, size_t column, size_t row, uint16_t width, uint16_t height, uint8_t *out_data, uint8_t type){

    uint16_t end_row;
    uint16_t end_column;

    scan_get_block_size(column, row, width, height, &end_column, &end_row);

    size_t read_pointer = 0;
    if(type == SCAN_BLOCK_HORIZONTALY){

        // read block horizontaly
        for(size_t i=row;i<end_row;i++){
            for(size_t j=column;j<end_column;j++){
                out_data[read_pointer] = in_data[i*width+j];
                read_pointer++;
            }
        }

    } else{

        // read block verticaly
        for(size_t i=column;i<end_column;i++){
            for(size_t j=row;j<end_row;j++){
                out_data[read_pointer] = in_data[j*width+i];
                read_pointer++;
            }
        }

    }

    return (end_column-column)*(end_row-row);

}

/*
 * write data of block to image array
 *
 * in_data - block array
 * out_data - image array
 * column - column of block
 * row - row of block
 * width - width of image
 * height - height of image
 * type - write horizontaly or verticaly
 * return size of writed data
 */
size_t scan_write_block(uint8_t *in_data, uint8_t *out_data, size_t column, size_t row, uint16_t width, uint16_t height, uint8_t type){

    uint16_t end_row;
    uint16_t end_column;

    scan_get_block_size(column, row, width, height, &end_column, &end_row);

    size_t write_pointer = 0;
    if(type == SCAN_BLOCK_HORIZONTALY){

        // read block horizontaly
        for(size_t i=row;i<end_row;i++){
            for(size_t j=column;j<end_column;j++){
                out_data[i*width+j] = in_data[write_pointer];
                write_pointer++;
            }
        }

    } else{

        // read block verticaly
        for(size_t i=column;i<end_column;i++){
            for(size_t j=row;j<end_row;j++){
                out_data[j*width+i] = in_data[write_pointer];
                write_pointer++;
            }
        }

    }

    return (end_column-column)*(end_row-row);
}

/*
 * code or decode input data with scan
 *
 * buffer - input data array
 * size_of_data - size of input data
 * out_buffer - output data
 * code - if true code with scan otherwise decode
 * dynamic - if true use adaptive scan
 * width - width of image
 * return byte size of coded data, coded data is returned in buffer
 */
size_t scan(uint8_t *buffer, size_t size_of_data, uint8_t **out_buffer, bool code, bool dynamic, size_t width){

    if(code){

        size_t height = (size_of_data-MODEL_HEADER)/width;

        if(height > UINT16_MAX){
            write(STD_ERR, "Input image height is bigger then 65535.\n", 41);
            exit(EXIT_FAILURE);
        }

        if(dynamic){

            *out_buffer = malloc(sizeof(uint8_t)*(size_of_data-1)*2+SCAN_HEADER_SIZE+((width/SCAN_BLOCK+1)*(height/SCAN_BLOCK+1)));
            if(*out_buffer == NULL){
                write(STD_ERR, "Out of memory.\n", 15);
                exit(EXIT_FAILURE);
            }
            
            // for each block
            size_t writed = SCAN_HEADER_SIZE;
            for(size_t i=0;i<height;i+=SCAN_BLOCK){
                for(size_t j=0;j<width;j+=SCAN_BLOCK){

                    uint8_t horizontal_block[SCAN_BLOCK_SIZE];
                    uint8_t vertical_block[SCAN_BLOCK_SIZE];
                    size_t readed_block;

                    // headers
                    uint8_t horizontal_block_encoded[SCAN_BLOCK_SIZE*2+SCAN_BLOCK_HEADER];
                    uint8_t vertical_block_encoded[SCAN_BLOCK_SIZE*2+SCAN_BLOCK_HEADER];

                    // out arrays
                    horizontal_block_encoded[0] = SCAN_BLOCK_HORIZONTALY;
                    vertical_block_encoded[0] = SCAN_BLOCK_VERTICALY;

                    // read block verticaly and horizontaly
                    readed_block = scan_read_block(buffer+MODEL_HEADER, j, i, width, height, horizontal_block, SCAN_BLOCK_HORIZONTALY);
                    scan_read_block(buffer+MODEL_HEADER, j, i, width, height, vertical_block, SCAN_BLOCK_VERTICALY);

                    size_t horizontaly_encoded_size;
                    size_t vertical_encoded_size;

                    // encode block horizontaly and verticaly
                    horizontaly_encoded_size = scan_static_encode(horizontal_block, readed_block, horizontal_block_encoded+SCAN_BLOCK_HEADER);
                    vertical_encoded_size = scan_static_encode(vertical_block, readed_block, vertical_block_encoded+SCAN_BLOCK_HEADER);

                    // write block to out
                    if(vertical_encoded_size >= horizontaly_encoded_size && horizontaly_encoded_size <= SCAN_BLOCK_SIZE){
                        memcpy(*out_buffer+writed, horizontal_block_encoded, horizontaly_encoded_size+SCAN_BLOCK_HEADER);
                        writed += horizontaly_encoded_size+SCAN_BLOCK_HEADER;
                    }else if(vertical_encoded_size <= SCAN_BLOCK_SIZE){
                        memcpy(*out_buffer+writed, vertical_block_encoded, vertical_encoded_size+SCAN_BLOCK_HEADER);
                        writed += vertical_encoded_size+SCAN_BLOCK_HEADER;
                    } else{
                        (*out_buffer+writed)[0] = SCAN_BLOCK_NONE;
                        memcpy(*out_buffer+writed+SCAN_BLOCK_HEADER, horizontal_block, SCAN_BLOCK_SIZE);
                        writed += SCAN_BLOCK_SIZE+SCAN_BLOCK_HEADER;
                    }
                }
            }

            // write new header
            scan_write_header(buffer, *out_buffer, width, height, SCAN_DYNAMIC);

            return writed;

        } else{

            // malloc out array
            *out_buffer = malloc(sizeof(uint8_t)*(size_of_data-1)*2+SCAN_HEADER_SIZE);
            if(*out_buffer == NULL){
                write(STD_ERR, "Out of memory.\n", 15);
                exit(EXIT_FAILURE);
            }

            // encode data
            size_t encoded_data_size;
            encoded_data_size = scan_static_encode(buffer+MODEL_HEADER, size_of_data-MODEL_HEADER, *out_buffer+SCAN_HEADER_SIZE);

            if(encoded_data_size+SCAN_HEADER_SIZE > size_of_data){

                //write(STD_ERR, "WARNING: static scan is inefective for this image and will be skiped.\n", 69);
                memcpy(*out_buffer+SCAN_HEADER_SIZE, buffer+MODEL_HEADER, size_of_data-MODEL_HEADER);
                scan_write_header(buffer, *out_buffer, width, height, SCAN_NONE);
                return size_of_data + SCAN_HEADER_SIZE - MODEL_HEADER;

            }

            // write new header
            scan_write_header(buffer, *out_buffer, width, height, SCAN_STATIC);

            return encoded_data_size + SCAN_HEADER_SIZE;
        }

    } else{

        uint16_t width;
        uint16_t height;
        uint8_t type;

        if(size_of_data < SCAN_BLOCK_HEADER){
            write(STD_ERR, "Input file corrupted.\n", 22);
            exit(EXIT_FAILURE);
        }

        scan_read_header(buffer, &width, &height, &type);
        uint8_t *without_header = buffer+SCAN_HEADER_SIZE;
        size_t size_without_header = size_of_data-SCAN_HEADER_SIZE;

        // malloc array for output
        size_t out_array_size;
        out_array_size = sizeof(uint8_t)*width*height+SCAN_HEADER_SIZE+1;
        *out_buffer = malloc(out_array_size);
        if(*out_buffer == NULL){
            write(STD_ERR, "Out of memory.\n", 15);
            exit(EXIT_FAILURE);
        }

        if(type == SCAN_STATIC){

            // decode data
            size_t decoded_data_size;
            decoded_data_size = scan_static_decode(without_header-MODEL_HEADER, size_without_header+MODEL_HEADER, *out_buffer, out_array_size, MODEL_HEADER);
            (*out_buffer)[0] = buffer[0];
            return decoded_data_size;

        } else if(type == SCAN_DYNAMIC){

            // for each block
            size_t readed = SCAN_HEADER_SIZE;

            for(size_t i=0;i<height;i+=SCAN_BLOCK){
                for(size_t j=0;j<width;j+=SCAN_BLOCK){

                    // decode data
                    uint8_t decoded_block[SCAN_BLOCK_SIZE+SCAN_BLOCK_HEADER];

                    // get size of block
                    uint16_t end_width;
                    uint16_t end_height;
                    scan_get_block_size(j, i, width, height, &end_width, &end_height);
                    size_t block_size = (j-end_width)*(i-end_height);

                    if((buffer+readed)[0] == SCAN_BLOCK_NONE){

                        // none scan block
                        scan_write_block(buffer+readed+SCAN_BLOCK_HEADER, *out_buffer+MODEL_HEADER, j, i, width, height, SCAN_BLOCK_HORIZONTALY);
                        readed += SCAN_BLOCK_SIZE+SCAN_BLOCK_HEADER;

                    } else{

                        // scan block
                        readed += scan_static_decode(buffer+readed, size_of_data-readed, decoded_block, block_size, SCAN_BLOCK_HEADER);
                        if(decoded_block[0] == SCAN_BLOCK_HORIZONTALY){
                            scan_write_block(decoded_block+SCAN_BLOCK_HEADER, *out_buffer+MODEL_HEADER, j, i, width, height, SCAN_BLOCK_HORIZONTALY);
                        } else{
                            scan_write_block(decoded_block+SCAN_BLOCK_HEADER, *out_buffer+MODEL_HEADER, j, i, width, height, SCAN_BLOCK_VERTICALY);
                        }

                    }
                }
            }

            (*out_buffer)[0] = buffer[0];

            return width*height+MODEL_HEADER;

        } else if(type == SCAN_NONE){

            memcpy(*out_buffer+MODEL_HEADER, without_header, size_without_header);
            (*out_buffer)[0] = buffer[0];
            return size_without_header+MODEL_HEADER;

        }
    }
}