////////////////////////////////////////////////
// KKO                            KKO project //
// 05.5.2023       Vojtěch Kulíšek (xkulis03) //
// Write output by bytes                      //
////////////////////////////////////////////////

uint8_t *bite_write_byte_pointer = NULL;
uint8_t bite_write_bit_pointer = 0;
size_t bite_writed_bytes = 0;

/*
 * inicialize array of uint8_t for bits write
 * pointer - pointer to start of array
 */
void bite_write_init(uint8_t *pointer){
    bite_write_byte_pointer = pointer;
    bite_write_bit_pointer = 0;
    bite_writed_bytes = 0;
}

/*
 * write byts to array of uint8_t
 * 
 * data - bits to write
 * size - number of bits to write
 */
void bite_write(uint8_t data, uint8_t size){

    if(bite_write_bit_pointer == 0){
        bite_write_byte_pointer[bite_writed_bytes] = 0;
    }

    uint8_t avalible_bits = 8-bite_write_bit_pointer;

    // if byte aliasing
    if(size > avalible_bits){

        // write to old cell
        bite_write_byte_pointer[bite_writed_bytes] |= data>>(size-avalible_bits);
        bite_writed_bytes += 1;
        bite_write_byte_pointer[bite_writed_bytes] = 0;
        // write to new cell
        bite_write_byte_pointer[bite_writed_bytes] |= data<<(8-(size)+avalible_bits);
        bite_write_bit_pointer = size-avalible_bits+1;
    } else{
        bite_write_byte_pointer[bite_writed_bytes] |= data<<(avalible_bits-size);
        bite_write_bit_pointer += size;

        // next byte
        if(bite_write_bit_pointer == 8){
            bite_write_bit_pointer = 0;
            bite_writed_bytes += 1;
        }
    }
}

/*
 * get numner of bytes writed to uint8_t array
 */
size_t bite_write_get_bite_writed_bytes(){
    
    if(bite_write_bit_pointer){
        return bite_writed_bytes+1;
    }

    return bite_writed_bytes;
}