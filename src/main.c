////////////////////////////////////////////////
// KKO                            KKO project //
// 05.5.2023       Vojtěch Kulíšek (xkulis03) //
// Parse arguments                            //
////////////////////////////////////////////////

#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <stdbool.h>
#include <inttypes.h>
#include <string.h>

#include <stdio.h>
#include <limits.h>

#define STD_OUT 0
#define STD_ERR 2
#define ERROR -1

#include "file.c"
#include "bite_write.c"
#include "bit_read.c"
#include "model.c"
#include "scan.c"
#include "huffman_tree.c"
#include "huffman.c"

int main(int argc, char *argv[]){

    // parse parametrs

    bool code = false;
    bool decode = false;
    bool use_model = false;
    bool adaptive_scan = false;
    bool help = false;
    char *in_file = NULL;
    char *out_file = NULL;
    size_t image_width = ERROR;

    bool read_in_file = false;
    bool read_out_file = false;
    bool read_width = false;

    for(size_t i=1;i<argc;i++){
        if(argv[i][0] == '-'){
            for(size_t j=1;argv[i][j] != '\0';j++){
                
                if(read_in_file || read_out_file){
                    write(STD_ERR, "Missing file path.\n", 19);
                    return EXIT_FAILURE;
                }
                if(read_width){
                    write(STD_ERR, "Missing image width.\n", 21);
                    return EXIT_FAILURE;
                }

                if(argv[i][j] == 'c'){code = true;}
                else if(argv[i][j] == 'd'){decode = true;}
                else if(argv[i][j] == 'm'){use_model = true;}
                else if(argv[i][j] == 'a'){adaptive_scan = true;}
                else if(argv[i][j] == 'i'){read_in_file = true;}
                else if(argv[i][j] == 'o'){read_out_file = true;}
                else if(argv[i][j] == 'w'){read_width = true;}
                else if(argv[i][j] == 'h'){help = true;}
                else{
                    char error_message[] = "Unknown option - , for help use -h option.\n";
                    error_message[16] = argv[i][j];
                    write(2, error_message, 43);
                    return EXIT_FAILURE;
                }
            }
        } else{
            if(read_in_file){in_file = argv[i];}
            else if(read_out_file){out_file = argv[i];}
            else if(read_width){
                image_width = strtoumax(argv[i], NULL, 10);
                if(!image_width || image_width == ERROR || errno == ERANGE || image_width > UINT16_MAX){
                    write(STD_ERR, "Specifed input image width is too hight.\n", 41);
                    return EXIT_FAILURE;
                }
            }
            else{
                write(STD_ERR, "Unaccepted value in arguments, for help use -h option.\n", 55);
                return EXIT_FAILURE;
            }
            read_in_file = false;
            read_out_file = false;
            read_width = false;
        }
    }

    if(read_in_file || read_out_file){
        write(STD_ERR, "Missing file path.\n", 19);
        return EXIT_FAILURE;
    }
    if(read_width){
        write(STD_ERR, "Missing image width.\n", 21);
        return EXIT_FAILURE;
    }

    // check for mismatch in arguments

    if(code == true && decode == true){
        write(STD_ERR, "Cannot code and decode in same time, for help use -h option.\n", 61);
        return EXIT_FAILURE;
    }
    if(!code && !decode && !help){
        write(STD_ERR, "Compress or decompress option was not specifed, for help use -h option.\n", 72);
        return EXIT_FAILURE;
    }

    if(code && image_width == ERROR){
        write(STD_ERR, "Image width not specifed.\n", 26);
        return EXIT_FAILURE;
    }

    if((!code && use_model) || (!code && adaptive_scan)){
        write(STD_ERR, "WARNING: [-ma] is ignored in decompression mode.\n", 49);
    }

    // print help if is speciffed in arguments

    if(help){
        write(STD_OUT, "\
Usage:	huff_codec -c -i <input file> -o <output file> -w <image width> [-ma]\n\
	huff_codec -d -i <input file> -o <output file>\n\
	huff_codec -h\n\
Compress or decompress grayscale images.\n\
\n\
Options:\n\
	-c		compress image\n\
	-d		decompress image\n\
	-m		use use_model\n\
	-a		use adaptive scanning\n\
	-i <file>	input file for compress/decompress\n\
	-o <file>	output file\n\
	-w		input image width\n\
	-h		print help\n", 387);
        return EXIT_SUCCESS;
    }

    // chek if files is specifed
    if(in_file == NULL){
        write(STD_ERR, "Input file is not specifed.\n", 28);
        return EXIT_SUCCESS;
    }
    if(out_file == NULL){
        write(STD_ERR, "Output file is not specifed.\n", 29);
        return EXIT_SUCCESS;
    }

    // read input file
    size_t input_file_size = get_file_size(in_file);

    if(input_file_size % image_width && code){
        write(STD_ERR, "Image width specifed in arguments not match with width of image in input file.\n", 79);
        return EXIT_FAILURE;
    }

    uint8_t *input_file_buffer = malloc((input_file_size+1)*sizeof(uint8_t));
    if(input_file_buffer == NULL){
        write(STD_ERR, "Out of memory.\n", 15);
        return EXIT_FAILURE;
    }
    
    // code or decode image

    size_t out_file_size = 0;
    size_t out_scan_size = 0;
    uint8_t *scan_out_buffer;
    uint8_t *out_buffer;
    if(code){
        // read file without first byte, first byte is model and scan header 
        read_file(in_file, input_file_size, input_file_buffer+1);
        model(input_file_buffer, input_file_size, true, use_model);

        out_scan_size = scan(input_file_buffer, input_file_size+1, &scan_out_buffer, true, adaptive_scan, image_width);
        free(input_file_buffer);

        out_file_size = huffman(scan_out_buffer, out_scan_size, &out_buffer, true);
        free(scan_out_buffer);
        
        write_to_file(out_file, out_file_size, out_buffer);
        free(out_buffer);
    }else{

        // read file with header
        read_file(in_file, input_file_size, input_file_buffer);

        out_file_size = huffman(input_file_buffer, input_file_size, &out_buffer, false);
        free(input_file_buffer);

        out_scan_size = scan(out_buffer, out_file_size, &scan_out_buffer, false, adaptive_scan, image_width);
        free(out_buffer);

        model(scan_out_buffer, out_scan_size, false, true);

        write_to_file(out_file, out_scan_size-sizeof(uint8_t), scan_out_buffer+sizeof(uint8_t));
        free(scan_out_buffer);
    }
    

    return EXIT_SUCCESS;
}