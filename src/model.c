////////////////////////////////////////////////
// KKO                            KKO project //
// 05.5.2023       Vojtěch Kulíšek (xkulis03) //
// Code or decode data with model             //
////////////////////////////////////////////////

/*
 * transform input data to model or
 * decode input data from model to raw data
 * output data is returned by data parametr
 *
 * data - input data
 * size - size of data array
 * code - if true use codding from raw data to model
 * use - if true use model not works for decoding
 */
void model(uint8_t *data, size_t size, bool code, bool use){

    if(code){

        // if is not allowed to use code
        if(!use){
            data[0] = 0; // file header do not use model
            return;
        }

        // code
        data[0] = 0b00000001;
        uint8_t prew_symbol = data[1];
        for(size_t i=1;i<size;i++){
            uint8_t actual_symbol = data[i+1];
            data[i+1] = data[i+1]-prew_symbol;
            prew_symbol = actual_symbol;
        }

    } else{

        // decode
        if(data[0] & 0b00000001){
            for(size_t i=1;i<size-1;i++){
                data[i+1] = data[i+1]+data[i];
            }
        }   

    }
}