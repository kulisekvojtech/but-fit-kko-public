////////////////////////////////////////////////
// KKO                            KKO project //
// 05.5.2023       Vojtěch Kulíšek (xkulis03) //
// Create huffman tree and read or write data //
////////////////////////////////////////////////

typedef struct huffman_tree {
    uint16_t left;
    uint16_t right;
    uint16_t parent;
    bool is_leaf;
    uint16_t size;
    uint8_t value;
} huffman_tree;

//511 max size for nodes (256^2-1)
huffman_tree huffman_tree_nodes[511];
uint16_t huffman_tree_end_pointer = 0;
uint16_t huffman_tree_pointer = 0;

/*
 * conver input to huffman code and write out
 * to buffer inicializt with bite_write_init()
 *
 * data - buffer with raw data
 * size - size of raw data in bytes
 */
void huffman_tree_convert_input_to_code_and_write_to_buffer(uint8_t *data, size_t size){

    uint8_t tree_cache[256];
    for(uint16_t i=0;huffman_tree_nodes[i].is_leaf;i++){
        tree_cache[huffman_tree_nodes[i].value] = i;
    }

    for(size_t i=0;i<size;i++){

        // get code
        uint8_t write_buffer[256];
        uint8_t pointer = 0;
        uint16_t prev_node = tree_cache[data[i]];
        huffman_tree node_for_path = huffman_tree_nodes[prev_node];
                
        while(node_for_path.size != 0){

            if(huffman_tree_nodes[node_for_path.parent].left == prev_node){
                write_buffer[pointer] = 0;
            }else{
                write_buffer[pointer] = 1;
            }
            pointer += 1;

            prev_node = node_for_path.parent;
            node_for_path = huffman_tree_nodes[node_for_path.parent];

        }

        // write code to buffer
        for(int16_t i=pointer;i>0;i--){
            bite_write(write_buffer[i-1], 1);
        }
    }
}

/*
 * conver input from huffman code and write out
 * to buffer inicialized with bite_write_init()
 *
 * in_data - array with encoded data
 * size - size of output data
 * out_data - output decoded data
 */
void huffman_convert_input_from_code(uint8_t *in_data, size_t size, uint8_t *out_data){
    
    read_bit_init(in_data);
    for(size_t i=0;i<size;i++){
        huffman_tree node = huffman_tree_nodes[huffman_tree_end_pointer-1];
        while(!node.is_leaf){
            if(bit_read()){
                node = huffman_tree_nodes[node.right];
            } else{
                node = huffman_tree_nodes[node.left];
            }
        }
        out_data[i] = node.value;
    }

}

/*
 * create huffman tree from arrays with sizes of bits shorted
 *
 * symbol_array - array with symbols with ordered by bit_count_array
 * bit_count_array - array with bit count of symbols shorted
 */
void huffman_tree_create_from_values_array(uint8_t *symbol_array, uint16_t *bit_count_array){

    uint16_t max_depth = 0;

    // create all leafs
    for(uint16_t i=0;i<256;i++){
        
        // symbol is not used
        if(bit_count_array[i] == 0){
            continue;
        }
        huffman_tree leaf;
        leaf.is_leaf = true;
        leaf.size = bit_count_array[i];
        leaf.value = symbol_array[i];
        huffman_tree_nodes[huffman_tree_end_pointer] = leaf;
        huffman_tree_end_pointer += 1;

        if(bit_count_array[i] > max_depth){
            max_depth = bit_count_array[i];
        }
    }

    // create all others
    for(uint16_t i=max_depth;i>0;i--){
        for(int32_t j=huffman_tree_end_pointer-1;j>=0;j--){
            if(huffman_tree_nodes[j].size == i){

                // search two nodes 
                uint16_t first = j;
                j -= 1;
                while(huffman_tree_nodes[j].size != i){
                    j -= 1;
                    if(j < 0){
                        write(STD_ERR, "Input file corrupted.\n", 22);
                        exit(EXIT_FAILURE);
                    }
                }
                uint16_t second = j;

                // create new node
                huffman_tree new_node;
                new_node.right = first;
                new_node.left = second;
                new_node.is_leaf = false;
                new_node.size = i-1;

                huffman_tree_nodes[huffman_tree_end_pointer] = new_node;
                huffman_tree_nodes[first].parent = huffman_tree_end_pointer;
                huffman_tree_nodes[second].parent = huffman_tree_end_pointer;
                huffman_tree_end_pointer += 1;
            }

        }
    }
}