////////////////////////////////////////////////
// KKO                            KKO project //
// 05.5.2023       Vojtěch Kulíšek (xkulis03) //
// Read input by bytes                        //
////////////////////////////////////////////////

uint8_t *bit_byte_pointer;
uint8_t bit_read_bit_pointer = 0;
size_t bit_read_bytes = 0;

/*
 * inicialize array of uint8_t for bit read
 * pointer - pointer to start of array
 */
void read_bit_init(uint8_t *pointer){
    bit_byte_pointer = pointer;
    bit_read_bit_pointer = 0;
    bit_read_bytes = 0;
}

/*
 * read one bit from array inicialized with read_bit_init()
 *
 * return readed bit
 */
bool bit_read(){

    bool readed = bit_byte_pointer[bit_read_bytes] & (0b10000000>>bit_read_bit_pointer);

    bit_read_bit_pointer += 1;
    if(bit_read_bit_pointer > 7){
        bit_read_bit_pointer = 0;
        bit_read_bytes += 1;
    }

    return readed;
}