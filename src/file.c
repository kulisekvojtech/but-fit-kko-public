////////////////////////////////////////////////
// KKO                            KKO project //
// 05.5.2023       Vojtěch Kulíšek (xkulis03) //
// Read or write file                         //
////////////////////////////////////////////////

#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>

/*
 * return file size if succes
 * otherwise print error and exit
 *
 * path - file path
 */
size_t get_file_size(char *path){

    struct stat file_info;
    
    if(stat(path, &file_info) == ERROR){
        if(errno == EACCES){
            write(STD_ERR, "Can not read input file, permission denied.\n", 44);
        } else if(errno == ENOENT){
            write(STD_ERR, "Can not read input file, file not exists.\n", 42);
        } else{
            write(STD_ERR, "stat() failed.\n", 15);
        }
        exit(EXIT_FAILURE);
    }

    // if is not directory
    if(S_ISDIR(file_info.st_mode)){
        write(STD_ERR, "Expected file not directory.\n", 29);
        exit(EXIT_FAILURE);
    }

    return file_info.st_size;
}

/*
 * read whole file with one system call
 * if file can not be readed, prints error
 * and exit with error code
 *
 * path - file path
 * size - max size for read
 * out - out buffer with readed data
 * return number of bytes readed by kernel
 */
size_t read_file(char *path, size_t size, uint8_t *out){

    int fd = open(path, O_RDONLY);

    if(fd == ERROR){
        if(errno == EACCES){
            write(STD_ERR, "Can not read input file permission denied.\n", 43);
        } else if(errno == EISDIR){
            write(STD_ERR, "Excepted file as input not directory.\n", 38);
        }
        exit(EXIT_FAILURE);
    }

    ssize_t readded_by_kernel = read(fd, out, size);
    if(readded_by_kernel != size){
         write(STD_ERR, "Kernel returned unaxcepted return value from system read call.\n", 63);
         exit(EXIT_FAILURE);
    }
    return readded_by_kernel;
}
/*
 * write whole file with one system call
 * if file can not be writed, prints error and
 * and exit with error code
 *
 * path - file path
 * size - size of data to write
 * in - array with data to write
 */
void write_to_file(char *path, size_t size, uint8_t *in){

    int fd = open(path, O_WRONLY|O_CREAT|O_TRUNC, 0777);
    if(fd == ERROR){
        if(errno == EACCES){
            write(STD_ERR, "Can not create output file permission denied.\n", 46);
        }
        exit(EXIT_FAILURE);
    }

    if(write(fd, in, size) != size){
        write(STD_ERR, "Failed write to created file.\n", 30);
        exit(EXIT_FAILURE);
    }

    close(fd);
}